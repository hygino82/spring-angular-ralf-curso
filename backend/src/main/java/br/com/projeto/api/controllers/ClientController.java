package br.com.projeto.api.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto.api.dto.ClientDTO;
import br.com.projeto.api.dto.ClientInsertDTO;
import br.com.projeto.api.services.ClientService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/client")
@CrossOrigin(origins = "*")
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    public ResponseEntity<ClientDTO> createClient(@RequestBody @Valid ClientInsertDTO insertDTO) {
        ClientDTO dto = clientService.createClient(insertDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @GetMapping
    public ResponseEntity<Page<ClientDTO>> getAllClients(Pageable pageable) {
        Page<ClientDTO> pageDto = clientService.getAllClients(pageable);
        return ResponseEntity.status(HttpStatus.CREATED).body(pageDto);
    }

    @GetMapping("/{id}")
    ResponseEntity<ClientDTO> findClientById(@PathVariable Long id) {
        ClientDTO dto = clientService.findClientById(id);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PutMapping("/{id}")
    ResponseEntity<ClientDTO> updateClientById(@PathVariable Long id,
            @RequestBody @Valid ClientInsertDTO insertDTO) {
        ClientDTO dto = clientService.updateClientById(id, insertDTO);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClientById(@PathVariable Long id) {
        clientService.deleteClientById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
