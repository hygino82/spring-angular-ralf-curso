package br.com.projeto.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.projeto.api.models.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("SELECT obj FROM Client obj WHERE UPPER(obj.city) = UPPER(:city)")
    public List<Client> findByCity(String city);

    @Query("SELECT obj FROM Client obj WHERE obj.id = :id")
    public Optional<Client> findClientById(Long id);
}
