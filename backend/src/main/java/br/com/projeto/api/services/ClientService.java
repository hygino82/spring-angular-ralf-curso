package br.com.projeto.api.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projeto.api.dto.ClientDTO;
import br.com.projeto.api.dto.ClientInsertDTO;
import br.com.projeto.api.models.Client;
import br.com.projeto.api.repositories.ClientRepository;
import jakarta.validation.Valid;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Transactional
    public ClientDTO createClient(@Valid ClientInsertDTO insertDTO) {
        Client entity = new Client(insertDTO);
        entity = clientRepository.save(entity);
        return new ClientDTO(entity);
    }

    @Transactional(readOnly = true)
    public Page<ClientDTO> getAllClients(Pageable pageable) {
        Page<Client> page = clientRepository.findAll(pageable);
        return page.map(client -> new ClientDTO(client));
    }

    @Transactional(readOnly = true)
    public ClientDTO findClientById(Long id) {
        Optional<Client> entity = clientRepository.findClientById(id);

        if (!entity.isPresent()) {
            throw new IllegalArgumentException("Não existe um cliente com o id informado");
        }

        return new ClientDTO(entity.get());
    }

    @Transactional
    public ClientDTO updateClientById(Long id, @Valid ClientInsertDTO dto) {
        Client entity = clientRepository.getReferenceById(id);
        entity.setAge(dto.age());
        entity.setCity(dto.city());
        entity.setName(dto.name());
        entity = clientRepository.save(entity);

        return new ClientDTO(entity);
    }

    public void deleteClientById(Long id) {
        Optional<Client> entity = clientRepository.findById(id);

        if (entity.isPresent()) {
            clientRepository.delete(entity.get());
        } else {
            throw new IllegalArgumentException("Não existe um cliente com o id: " + id);
        }
    }
}
