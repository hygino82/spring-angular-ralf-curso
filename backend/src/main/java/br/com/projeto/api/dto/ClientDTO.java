package br.com.projeto.api.dto;

import br.com.projeto.api.models.Client;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ClientDTO {

        private Long id;
        private String name;
        private Integer age;
        private String city;

        public ClientDTO(Client entity) {
                id = entity.getId();
                name = entity.getName();
                age = entity.getAge();
                city = entity.getCity();
        }
}