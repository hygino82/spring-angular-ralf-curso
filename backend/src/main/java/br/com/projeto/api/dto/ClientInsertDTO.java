package br.com.projeto.api.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record ClientInsertDTO(
        @NotEmpty String name,
        @NotNull Integer age,
        @NotEmpty String city) {
}
